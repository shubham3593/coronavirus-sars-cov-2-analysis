#!/usr/bin/env python
# coding: utf-8

# # SARS-CoV-2 Analysis .
# ### By- Shubham Kumar
# ### Date- May 09,2021
# Steps:
#     1-First we need to gather data.
#     2-Arrange the data as per need as we have mixed format data.
#     3-Proceed with analysis part.

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import style
get_ipython().run_line_magic('matplotlib', 'inline')

import plotly
import plotly.express as px
import plotly.graph_objects as go
plt.rcParams['figure.figsize']=20,12
import cufflinks as cf
import plotly.offline as pyo
from plotly.offline import init_notebook_mode,plot,iplot

import folium


# In[2]:


pyo.init_notebook_mode(connected=True)
cf.go_offline()


# In[3]:


df=pd.read_excel(r"/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/Covid cases in India.xls")


# In[4]:


df


# In[5]:


df.drop(['S. No.'],axis=1,inplace=True)


# In[6]:


df


# In[7]:


df['Total cases']=df['Total Confirmed cases (Indian National)']+df['Total Confirmed cases ( Foreign National )']


# In[8]:


df


# In[9]:


df['Total cases'].sum()


# In[10]:


df['Active Cases']=df['Total cases']-(df['Death']+df['Cured'])


# In[11]:


df


# In[12]:


df.style.background_gradient(cmap='Reds')


# In[13]:


Total_Active_Cases=df.groupby('Name of State / UT')['Active Cases'].sum().sort_values(ascending=False).to_frame()


# In[14]:


Total_Active_Cases


# In[15]:


Total_Active_Cases.style.background_gradient(cmap='Reds')


# In[16]:


df.plot(kind='bar',x='Name of State / UT',y='Total cases')
plt.show()
df.iplot(kind='bar',x='Name of State / UT',y='Total cases')


# In[17]:


plt.bar(df['Name of State / UT'],df['Total cases'])


# In[18]:


px.bar(df,x='Name of State / UT',y='Total cases')


# In[19]:


df.plot(kind='scatter',x='Name of State / UT',y='Total cases')


# In[20]:


plt.scatter(df['Name of State / UT'],df['Total cases'])


# In[21]:


df.iplot(kind='scatter',x='Name of State / UT',y='Total cases',mode='markers+lines',title='My Graph',xTitle='Name of State / UT',yTitle='Total cases',colors='red',size=20)
px.scatter(df,x='Name of State / UT',y='Total cases')


# In[22]:


fig=plt.figure(figsize=(20,10),dpi=200)
axes=fig.add_axes([0,0,1,1])
axes.bar(df['Name of State / UT'],df['Total cases'])
axes.set_title("Total Cases in India")
axes.set_xlabel("Name of State / UT")
axes.set_ylabel("Total cases")
plt.show()
fig=go.Figure()
fig.add_trace(go.Bar(x=df['Name of State / UT'],y=df['Total cases']))
fig.update_layout(title='Total Cases in India',xaxis=dict(title='Name of State / UT'),yaxis=dict(title='Total cases'))


# In[23]:


Indian_Cord=pd.read_excel(r"/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/Indian Coordinates.xls")


# In[24]:


Indian_Cord


# In[25]:


df_full=pd.merge(Indian_Cord,df,on='Name of State / UT')


# In[26]:


df_full


# In[27]:


map=folium.Map(location=[20,70],zoom_start=4,tiles='Stamenterrain')
for lat,long,value, name in zip(df_full['Latitude'],df_full['Longitude'],df_full['Total cases'],df_full['Name of State / UT']):
    folium.CircleMarker([lat,long],radius=value*0.8,popup=('<strong>State</strong>: '+str(name).capitalize()+'<br>''<strong>Total cases</strong>: ' + str(value)+ '<br>'),color='red',fill_color='red',fill_opacity=0.3).add_to(map)


# In[28]:


map


# In[29]:


dbd_India=pd.read_excel(r"/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/per_day_cases.xls",parse_dates=True,sheet_name="India")
dbd_Italy=pd.read_excel(r"/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/per_day_cases.xls",parse_dates=True,sheet_name="Italy")
dbd_Korea=pd.read_excel(r"/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/per_day_cases.xls",parse_dates=True,sheet_name="Korea")
dbd_Wuhan=pd.read_excel(r"/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/per_day_cases.xls",parse_dates=True,sheet_name="Wuhan")


# In[30]:


dbd_India


# In[31]:


fig=plt.figure(figsize=(10,5),dpi=200)
axes=fig.add_axes([0.1,0.1,0.8,0.8])
axes.bar(dbd_India["Date"],dbd_India["Total Cases"],color='blue')
axes.set_xlabel("Date")
axes.set_ylabel("Total Cases")
axes.set_title("Confirmed cases in India")
plt.show()
fig=px.bar(dbd_India,x="Date",y="Total Cases",color='Total Cases',title='Confirmed cases in India')
fig.show()


# In[32]:


fig=px.bar(dbd_Italy,x="Date",y="Total Cases",color='Total Cases',title='Confirmed cases in Italy')
fig.show()


# In[33]:


fig=px.bar(dbd_Korea,x="Date",y="Total Cases",color='Total Cases',title='Confirmed cases in Korea')
fig.show()


# In[34]:


fig=px.bar(dbd_Wuhan,x="Date",y="Total Cases",color='Total Cases',title='Confirmed cases in Wuhan')
fig.show()


# In[35]:


fig=plt.figure(figsize=(10,5),dpi=200)
axes=fig.add_axes([0.1,0.1,0.8,0.8])
axes.plot(dbd_India["Date"],dbd_India["Total Cases"],color='blue',marker='*')
axes.set_xlabel("Date")
axes.set_ylabel("Total Cases")
axes.set_title("Confirmed cases in India")
plt.show()
fig=px.scatter(dbd_India,x="Date",y="Total Cases",color='Total Cases',title='Confirmed cases in India')
fig.show()


# In[36]:


dbd_India.iplot(kind='scatter',x='Date',y='Total Cases',mode='lines+markers')


# In[37]:


fig=go.Figure()
fig.add_trace(go.Scatter(x=dbd_India['Date'],y=dbd_India['Total Cases'],mode='lines+markers'))


# In[38]:


from plotly.subplots import make_subplots


# In[39]:


fig=make_subplots(
    rows=2,cols=2,
    specs=[[{"secondary_y":True},{"secondary_y":True}],[{"secondary_y":True},{"secondary_y":True}]],
    subplot_titles=("S.Korea","Italy","India","Wuhan"))

fig.add_trace(go.Bar(x=dbd_Korea['Date'],y=dbd_Korea['Total Cases'],
                    marker=dict(color=dbd_Korea['Total Cases'],coloraxis="coloraxis")),1,1)

fig.add_trace(go.Bar(x=dbd_Italy['Date'],y=dbd_Italy['Total Cases'],
                    marker=dict(color=dbd_Italy['Total Cases'],coloraxis="coloraxis")),1,2)

fig.add_trace(go.Bar(x=dbd_India['Date'],y=dbd_India['Total Cases'],
                    marker=dict(color=dbd_India['Total Cases'],coloraxis="coloraxis")),2,1)

fig.add_trace(go.Bar(x=dbd_Wuhan['Date'],y=dbd_Wuhan['Total Cases'],
                    marker=dict(color=dbd_Wuhan['Total Cases'],coloraxis="coloraxis")),2,2)
fig.update_layout(coloraxis=dict(colorscale='Bluered_r'),showlegend=False,title_text="Total Cases in 4 Countries")
fig.update_layout(plot_bgcolor='rgb(230,230,230)')


# In[40]:


fig=make_subplots(
    rows=2,cols=2,
    specs=[[{"secondary_y":True},{"secondary_y":True}],[{"secondary_y":True},{"secondary_y":True}]],
    subplot_titles=("S.Korea","Italy","India","Wuhan"))

fig.add_trace(go.Scatter(x=dbd_Korea['Date'],y=dbd_Korea['Total Cases'],
                    marker=dict(color=dbd_Korea['Total Cases'],coloraxis="coloraxis")),1,1)

fig.add_trace(go.Scatter(x=dbd_Italy['Date'],y=dbd_Italy['Total Cases'],
                    marker=dict(color=dbd_Italy['Total Cases'],coloraxis="coloraxis")),1,2)

fig.add_trace(go.Scatter(x=dbd_India['Date'],y=dbd_India['Total Cases'],
                    marker=dict(color=dbd_India['Total Cases'],coloraxis="coloraxis")),2,1)

fig.add_trace(go.Scatter(x=dbd_Wuhan['Date'],y=dbd_Wuhan['Total Cases'],
                    marker=dict(color=dbd_Wuhan['Total Cases'],coloraxis="coloraxis")),2,2)
fig.update_layout(coloraxis=dict(colorscale='Bluered_r'),showlegend=False,title_text="Total Cases in 4 Countries")
fig.update_layout(plot_bgcolor='rgb(230,230,230)')


# In[41]:


df=pd.read_csv('/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/covid_19_data.csv',parse_dates=['Last Update'])


# In[42]:


df.rename(columns={'ObservationDate':'Date','Country/Region':'Country'},inplace=True)


# In[43]:


df


# In[44]:


df.query('Country=="US"')


# In[45]:


df.groupby('Date').sum()


# In[46]:


confirmed=df.groupby('Date').sum()['Confirmed'].reset_index()
death=df.groupby('Date').sum()['Deaths'].reset_index()
rec=df.groupby('Date').sum()['Recovered'].reset_index()


# In[47]:


fig=go.Figure()
fig.add_trace(go.Scatter(x=confirmed['Date'],y=confirmed['Confirmed'],mode='lines+markers',name='Confirmed',line=dict(color='blue',width=2)))

fig.add_trace(go.Scatter(x=death['Date'],y=death['Deaths'],mode='lines+markers',name='Deaths',line=dict(color='red',width=2)))
fig.add_trace(go.Scatter(x=rec['Date'],y=rec['Recovered'],mode='lines+markers',name='Recovered',line=dict(color='green',width=2)))


# In[48]:


df_confirmed=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Covid Analysis using Python/Data/time_series_covid_19_confirmed.csv')


# In[49]:


df_confirmed.rename(columns={'Country/Region':'Country'},inplace=True)
df_latlong=pd.merge(df,df_confirmed,on=['Country','Province/State'])


# In[50]:


df_confirmed


# In[51]:


df_latlong


# In[52]:


fig=px.density_mapbox(df_latlong,lat="Lat",lon="Long",hover_name="Province/State",hover_data=["Confirmed","Deaths","Recovered"],animation_frame="Date",color_continuous_scale="Portland",radius=7,zoom=0,height=700)
fig.update_layout(title='Worldwide Corona Virus Cases')
fig.update_layout(mapbox_style="open-street-map",mapbox_center_lon=0)
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

