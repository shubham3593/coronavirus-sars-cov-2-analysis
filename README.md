# Coronavirus SARS-CoV-2 Analysis

Analysing SARS_CoV-2 from government provided datasets.

## Author

Shubham Kumar

## About

Basically this project mainly aims for Covid cases only for India as I didn't have accurate datasets for world and that's why I used previous datasets.
Datasets can be assessed from:
https://drive.google.com/drive/folders/1sIlViGRa5pRaq3HsL2GBnbHU6gjRzHpy?usp=sharing

Steps followed are as:
* Data gathering and uploading
* Data Wrangling
* Data Visualization
* EDA
Thankyou!

